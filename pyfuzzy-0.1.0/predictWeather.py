from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer

import sys, pickle


def predict_weather(name, year):
    resultdict = dict()
    atData = list()
    pData = list()


    ####################################################################################
    ######################            Pickle file name            ######################
    ####################################################################################
    atn = pickle.load( open( "WeatherPickles/" + name + "-avgTemp.pickle", "rb" ) )
    pn = pickle.load( open( "WeatherPickles/" + name + "-prec.pickle", "rb" ) )


    ####################################################################################
    ######################            DATASET CREATION            ######################
    ####################################################################################
    attrainingDataSet = SupervisedDataSet(12, 12)
    attestingDataSet = SupervisedDataSet(12, 12)
    ptrainingDataSet = SupervisedDataSet(12, 12)
    ptestingDataSet = SupervisedDataSet(12, 12)


    ####################################################################################
    #####################            Opening csv files            ######################
    ####################################################################################
    at = open('WeatherData/' + name + '-avgTemp.csv','r')
    p = open('WeatherData/' + name + '-prec.csv','r')


    #####################################################################################
    #Reading avg temp data for the given region and adding to a list after preprocessing#
    #####################################################################################
    l = int("1")
    for line in at.readlines():
        try:
            if l >= 3:
                data = [float(x[1:-1]) for x in line.strip().split('\t') if x != '']
                atData.append(data[1:])
            l = l + 1
        except ValueError,e:
                print "error",e,"on line"


    ##########################################################################################
    #Reading precipitation data for the given region and adding to a list after preprocessing#
    ##########################################################################################
    l = int("1")
    for line in p.readlines():
        try:
            if l >= 3:
                data = [float(x[1:-1]) for x in line.strip().split('\t') if x != '']
                pData.append(data[1:])
            l = l + 1
        except ValueError,e:
                print "error",e,"on line"


    ####################################################################################
    ######   If they are not equal, that means somewhere the data is incomplete  #######
    ####################################################################################
    if len(pData) != len(atData):
        print "Problem or Exception in data"
        sys.exit()
        

    ####################################################################################
    #################          Predicting the avg temp result        ###################
    ####################################################################################
    input = atData[len(atData) - (2016 - year) - 1]
    resultat = atn.activate(input)
    resultdict["avgTemp"] = resultat


    ####################################################################################
    ###############          Predicting the precipitation result        ################
    ####################################################################################
    input = pData[len(pData) - (2016 - year) - 1]
    resultp = pn.activate(input)
    resultdict["precipitation"] = resultp

    return resultdict

