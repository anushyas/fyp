__author__ = 'SONY'
from collections import defaultdict
import pickle

crop_data = defaultdict(list)

f = open( 'nutrients.csv', 'rU' ) #open the file in read universal mode
for line in f:
    cells = line.split( "," )
    crop_data[cells[0].lower()].append((cells[ 7 ], cells[ 8 ], cells[ 9 ], cells[1], cells[2]))
f.close()
del crop_data['']
del crop_data['crop']

with open('cropdata.pickle', 'wb') as handle:
  pickle.dump(crop_data, handle, protocol = 0)

print(crop_data["rice".lower()])


#read synonyms
with open("synonyms.txt","r") as f:
	crop_syns = f.readlines()
crop_syn_dict=list()
for crop in crop_syns:
	crop_syn_dict.append(filter(None,crop.lower().strip().split(";")))
# print crop_syn_dict

with open('cropsyn.pickle','wb') as ch:
	pickle.dump(crop_syn_dict, ch, protocol = 0)
