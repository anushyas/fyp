import csv
import pickle

def extract_pest_data():
    mylist = list(csv.reader(open("pesticides.csv"), skipinitialspace=True))
    del mylist[0]
    pest = dict()
    for item in mylist:
        pest[item[0].lower()] = (item[1], item[2], item[3])
    # del pest['']
    print pest
    with open('pesticide_data.pickle', 'wb') as handle:
        pickle.dump(pest, handle)

    #pest crop syn
    with open('pest_crop_syn.txt') as f:
        pest_crop_syns = f.readlines()
    crop_syn_dict=list()
    for crop in pest_crop_syns:
        crop_syn_dict.append(filter(None,crop.lower().strip().split(";")))
    with open('pestcropsyn.pickle','wb') as ch:
        pickle.dump(crop_syn_dict, ch)


def pest_recommend(crop):
    #crop name pre process
    with open('fertpest/pestcropsyn.pickle', 'rb') as ch:
        crop_syn_list = pickle.load(ch)
    for scrop in crop_syn_list:
        for syn in scrop:
            if(crop==syn):
                crop = scrop[0]
                break;
    with open('fertpest/pesticide_data.pickle', 'rb') as handle:
        pest = pickle.load(handle)
        if crop in pest:
            return list(pest[crop])
        else:
            return list()
# extract_pest_data()