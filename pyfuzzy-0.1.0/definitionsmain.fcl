FUNCTION_BLOCK dummy

    VAR_INPUT
        temp :     REAL; (* RANGE(10 .. 40) *)
        rain:    REAL; (* RANGE(300 .. 4000) *)
        ph:    REAL; (* RANGE(4.5 .. 8.5) *)
    END_VAR

    VAR_OUTPUT
        crop : REAL; (* RANGE(1 .. 31) *)
    END_VAR

	FUZZIFY temp
        TERM t25 := (23, 0) (25, 1) (27, 0) ;
        TERM t10to25 := (10, 0) (17, 1) (19, 1) (25, 0) ;
        TERM t10to30 := (10, 0) (19, 1) (21, 1) (30, 0) ;
        TERM t10to40 := (10, 0) (23, 1) (27, 1) (40, 0) ;
        TERM t12to16 := (12, 0) (14, 1) (16, 0) ;
        TERM t13to24 := (13, 0) (18, 1) (19, 1) (24, 0) ;
        TERM t15to35 := (15, 0) (23, 1) (27, 1) (35, 0) ;
        TERM t16to21 := (16, 0) (18, 1) (19, 1) (21, 0) ;
        TERM t20to25 := (20, 0) (23, 1) (25, 0) ;
        TERM t20to30 := (20, 0) (24, 1) (26, 1) (30, 0) ;
        TERM t20to34 := (20, 0) (26, 1) (28, 1) (34, 0) ;
        TERM t20to40 := (20, 0) (28, 1) (32, 1) (40, 0) ;
        TERM t22to32 := (22, 0) (27, 1) (28, 1) (32, 0) ;
        TERM t24to30 := (24, 0) (27, 1) (30, 0) ;
        TERM t25to30 := (25, 0) (27.5, 1) (30, 0) ;
        TERM t25to32 := (25, 0) (28, 1) (29, 1) (32, 0) ;
        TERM t25to35 := (25, 0) (29, 1) (31, 1) (35, 0) ;
        TERM t26to30 := (26, 0) (28, 1) (30, 0) ;
        TERM t26to32 := (26, 0) (28, 1) (29, 1) (32, 0) ;
        TERM t30to32 := (30, 0) (31, 1) (32, 0) ;
        TERM t30to34 := (30, 0) (32, 1) (34, 0) ;
        TERM t30to35 := (30, 0) (33, 1) (35, 0) ;
    END_FUZZIFY

    FUZZIFY rain
        TERM r400 := (350, 0) (400, 1) (450, 0) ;
        TERM r500 := (450, 0) (500, 1) (550, 0) ;
        TERM r1000 := (950, 0) (1000, 1) (1050, 0) ;
        TERM r1500 := (1450, 0) (1500, 1) (1550, 0) ;
        TERM r2000 := (1950, 0) (2000, 1) (2050, 0) ;
        TERM r1000to1750 := (1000, 0) (1350, 1) (1400, 1) (1750, 0) ;
        TERM r1300to3000 := (1300, 0) (2600, 1) (2700, 1) (3000, 0) ;
        TERM r1500to2000 := (1500, 0) (1725, 1) (1775, 1) (2000, 0) ;
        TERM r1600to2000 := (1600, 0) (1800, 1) (2000, 0) ;
        TERM r200to500 := (200, 0) (400, 1) (500, 0) ;
        TERM r400to600 := (400, 0) (500, 1) (600, 0) ;
        TERM r500to1000 := (500, 0) (725, 1) (775, 1) (1000, 0) ;
        TERM r500to600 := (500, 0) (550, 1) (600, 0) ;
        TERM r500to700 := (500, 0) (600, 1) (700, 0) ;
        TERM r500to900 := (500, 0) (750, 1)(900, 0) ;
        TERM r600to1000 := (600, 0) (800, 1) (1000, 0) ;
        TERM r600to650 := (600, 0) (625, 1) (650, 0) ;
        TERM r600to750 := (600, 0) (675, 1) (750, 0) ;
        TERM r600to900 := (600, 0) (750, 1) (900, 0) ;
        TERM r625to1000 := (625, 0) (812, 1) (1000, 0) ;
        TERM r630to760 := (630, 0) (695, 1) (760, 0) ;
        TERM r650to750 := (650, 0) (700, 1) (750, 0) ;
        TERM r700to1000 := (700, 0) (850, 1) (1000, 0) ;
        TERM r750to4500 := (750, 0) (2550, 1) (2700, 1) (4500, 0) ;
        TERM r800to1200 := (800, 0) (1000, 1) (1200, 0) ;
        TERM r850to1000 := (850, 0) (925, 1) (1000, 0) ;
    END_FUZZIFY

    FUZZIFY ph
        TERM p45to8 := (4.5, 0) (6, 1) (6.5, 1) (8, 0) ;
        TERM p48to58 := (4.8, 0) (5.2, 1) (5.4, 1) (5.8, 0) ;
        TERM p55to65 := (5.5, 0) (5.9, 1) (6.1, 1) (6.5, 0) ;
        TERM p55to7 := (5.5, 0) (6.2, 1) (6.3, 1) (7, 0) ;
        TERM p55to75 := (5.5, 0) (6.4, 1) (6.6, 1) (7.5, 0) ;
        TERM p55to8 := (5.5, 0) (6.7, 1) (6.9, 1) (8, 0) ;
        TERM p58to7 := (5.8, 0) (6.4, 1) (7, 0) ;
        TERM p5to6 := (5, 0) (5.5, 1) (6, 0) ;
        TERM p5to65 := (5, 0) (5.9, 1) (6.1, 1) (6.5, 0) ;
        TERM p5to85 := (5, 0) (6.7, 1) (6.9, 1) (8.5, 0) ;
        TERM p65to7 := (6.5, 0) (6.75, 1) (7, 0) ;
        TERM p65to75 := (6.5, 0) (6.9, 1) (7.1, 1) (7.5, 0) ;
        TERM p65to8 := (6.5, 0) (7.2, 1) (7.3, 1) (8, 0) ;
        TERM p65to85 := (6.5, 0) (7.4, 1) (7.6, 1) (8.5, 0) ;
        TERM p6to75 := (6, 0) (6.7, 1) (6.8, 1) (7.5, 0) ;
        TERM p6to8 := (6, 0) (6.9, 1) (7.1, 1) (8, 0) ;
    END_FUZZIFY

    DEFUZZIFY crop
        TERM arecanut := 1 ;
        TERM arhar := 2 ;
        TERM bajra := 3 ;
        TERM banana := 4 ;
        TERM barley := 5 ;
        TERM castorseeds := 6 ;
        TERM chilli := 7 ;
        TERM coconut := 8 ;
        TERM cotton := 9 ;
        TERM gram := 10 ;
        TERM groundnut := 11 ;
        TERM horsegram := 12 ;
        TERM jowar := 13 ;
        TERM jute := 14 ;
        TERM maize := 15 ;
        TERM mustard := 16 ;
        TERM onion := 17 ;
        TERM pea := 18 ;
        TERM ragi := 19 ;
        TERM rice := 20 ;
        TERM sesame := 21 ;
        TERM soyabean := 22 ;
        TERM sugarcane := 23 ;
        TERM sunflower := 24 ;
        TERM tobacco := 25 ;
        TERM turmeric := 26 ;
        TERM urad := 27 ;
        TERM wheat := 28 ;
        ACCU:MAX;
        METHOD: COGS;(*MoM;*)
        DEFAULT := 0; 
    END_DEFUZZIFY

    RULEBLOCK first
        AND:MIN;
        (*ACCU:MAX;*)
        RULE 0: IF (temp IS t10to40) AND (rain IS r750to4500) AND (ph IS p5to65) THEN (crop IS arecanut);
        RULE 1: IF (temp IS t30to35) AND (rain IS r600to650) AND (ph IS p65to75) THEN (crop IS arhar);
        RULE 2: IF (temp IS t20to25) AND (rain IS r600to650) AND (ph IS p65to75) THEN (crop IS arhar);
        RULE 3: IF (temp IS t20to30) AND (rain IS r400to600) AND (ph IS p6to8) THEN (crop IS bajra);
        RULE 4: IF (temp IS t15to35) AND (rain IS r650to750) AND (ph IS p65to75) THEN (crop IS banana);
        RULE 5: IF (temp IS t12to16) AND (rain IS r200to500) AND (ph IS p6to75) THEN (crop IS barley);
        RULE 6: IF (temp IS t30to32) AND (rain IS r200to500) AND (ph IS p6to75) THEN (crop IS barley);
        RULE 7: IF (temp IS t26to32) AND (rain IS r600to750) AND (ph IS p65to75) THEN (crop IS castorseeds);
        RULE 8: IF (temp IS t20to25) AND (rain IS r630to760) AND (ph IS p5to6) THEN (crop IS chilli);
        RULE 9: IF (temp IS t26to32) AND (rain IS r2000) AND (ph IS p5to65) THEN (crop IS coconut);
        RULE 10: IF (temp IS t25) AND (rain IS r1500to2000) AND (ph IS p65to8) THEN (crop IS cotton);
        RULE 11: IF (temp IS t25to35) AND (rain IS r850to1000) AND (ph IS p6to75) THEN (crop IS gram);
        RULE 12: IF (temp IS t24to30) AND (rain IS r700to1000) AND (ph IS p65to7) THEN (crop IS groundnut);
        RULE 13: IF (temp IS t20to34) AND (rain IS r500to700) AND (ph IS p6to75) THEN (crop IS horsegram);
        RULE 14: IF (temp IS t25to32) AND (rain IS r400) AND (ph IS p6to75) THEN (crop IS jowar);
        RULE 15: IF (temp IS t25to30) AND (rain IS r1600to2000) AND (ph IS p48to58) THEN (crop IS jute);
        RULE 16: IF (temp IS t22to32) AND (rain IS r500to900) AND (ph IS p55to7) THEN (crop IS maize);
        RULE 17: IF (temp IS t10to25) AND (rain IS r625to1000) AND (ph IS p6to75) THEN (crop IS mustard);
        RULE 18: IF (temp IS t13to24) AND (rain IS r500to600) AND (ph IS p65to75) THEN (crop IS onion);
        RULE 19: IF (temp IS t16to21) AND (rain IS r500to600) AND (ph IS p65to75) THEN (crop IS onion);
        RULE 20: IF (temp IS t30to35) AND (rain IS r500to600) AND (ph IS p65to75) THEN (crop IS onion);
        RULE 21: IF (temp IS t10to30) AND (rain IS r500) AND (ph IS p6to8) THEN (crop IS pea);
        RULE 22: IF (temp IS t30to34) AND (rain IS r1000) AND (ph IS p45to8) THEN (crop IS ragi);
        RULE 23: IF (temp IS t20to40) AND (rain IS r1300to3000) AND (ph IS p55to65) THEN (crop IS rice);
        RULE 24: IF (temp IS t25to30) AND (rain IS r500to600) AND (ph IS p55to8) THEN (crop IS sesame);
        RULE 25: IF (temp IS t26to32) AND (rain IS r600to750) AND (ph IS p6to75) THEN (crop IS soyabean);
        RULE 26: IF (temp IS t25to32) AND (rain IS r1000to1750) AND (ph IS p5to85)  THEN (crop IS sugarcane);
        RULE 27: IF (temp IS t24to30) AND (rain IS r600to900) AND (ph IS p65to85) THEN (crop IS sunflower);
        RULE 28: IF (temp IS t26to30) AND (rain IS r800to1200) AND (ph IS p55to65) THEN (crop IS tobacco);
        RULE 29: IF (temp IS t20to30) AND (rain IS r1500) AND (ph IS p58to7) THEN (crop IS turmeric);
        RULE 30: IF (temp IS t25to35) AND (rain IS r600to1000) AND (ph IS p55to75) THEN (crop IS urad);
        RULE 31: IF (temp IS t20to25) AND (rain IS r500to1000) AND (ph IS p65to75) THEN (crop IS wheat);

    END_RULEBLOCK

END_FUNCTION_BLOCK