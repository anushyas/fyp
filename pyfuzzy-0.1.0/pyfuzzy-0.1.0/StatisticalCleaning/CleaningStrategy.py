import collections,pickle
import operator
from getTitle import getTitle

crop_strategy = collections.defaultdict(list)
cstrategy = collections.defaultdict(list)
cstrategylength = dict()

key_terms = [
	"sow",
	"distance",
	"harvest",
	"days",
	"rainfall",
	"temperature",
	"month",
	"production",
	"gap",
	"space",
	"length",
	"plough",
	"soil",
	"inch",
	"disease",
	"time",
	"kg/ha",
	"land",
	"yield",
	"preparation",
	"prepare",
	"litre",
	"farm",
	"seed",
	"spray",
	"year",
	"week",
	"fertilizer",
	"herbicide",
	"fungicide"
	"pesticide"
	"weeding",
	"fruits",
	"season",
	"sunny",
	"rainy",
	"cultivate",
	"january",
	"february",
	"march",
	"april",
	"may",
	"june",
	"july",
	"august",
	"september",
	"october",
	"november",
	"december",
	"growth",
	"period",
	"grow",
	"crop",
	"texture",
	"kg",
	"humid",
	"moist",
	"condition",
	"irrigation",
	"percentage",
	"acre",
	"approximately",
	"worm",
	"water"
]

maxleng = 0.0

with open('crop_strategy.pickle', 'rb') as handle:
	crop_strategy = pickle.load(handle)
	for key, value in crop_strategy.items():
		if getTitle(key) not in cstrategy:
			cstrategy[getTitle(key)] = list()
		for ov in value:
			v = ' '.join(ov)
			v = v.lower()
			v = v.replace(" ","")
			addv = False
			for k in key_terms:
				if k in v:
					addv = True
			if addv and ov not in cstrategy[getTitle(key)]:
				cstrategy[getTitle(key)].append(ov)

		cstrategylength[getTitle(key)] = len(cstrategy[getTitle(key)])
		if cstrategylength[getTitle(key)] > maxleng:
				maxleng = cstrategylength[getTitle(key)]


	for key, value in cstrategylength.items():
		if maxleng != 0:
			cstrategylength[key] = float(cstrategylength[key]) / float(maxleng)

rank = dict()

for k, v in cstrategylength.items():
	rank[k] = v

print rank

with open('synonym_crops_strategy.pickle', 'wb') as handle:
	pickle.dump(cstrategy, handle)

with open('synonym_crops_rank.pickle', 'wb') as handle:
	pickle.dump(cstrategylength, handle)
