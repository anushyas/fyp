import urllib, collections, sys, csv, os
import xml.etree.ElementTree as ET

districts = {}

crop = sys.argv[1]
outputFileName = crop + ".csv"
tcount = 0

with open(outputFileName, 'w') as csvfile:
	fieldnames = ['year', 'price']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()

	for year in range(2005, 2016):
		url = "https://data.gov.in/sites/default/files/" + crop + "_" + str(year) + ".xml"
		fileName = crop + str(year) + ".xml"
		urllib.urlretrieve (url, fileName)
		print fileName
		try:
			tree = ET.parse(fileName)		
			root = tree.getroot()


			for child in root:
				for chil in child:
					for chi in chil:
						for ch in chi:
							for c in ch:
								r = c

			count = 0
			totalAvg = 0.0

			for child in r:

				if child.find('Min_x0020_Price') is None or child.find('Max_x0020_Price') is None:
					continue
				minPrice = float(child.find('Min_x0020_Price').text)
				maxPrice = float(child.find('Max_x0020_Price').text)
				avgPrice = (minPrice + maxPrice) / 2.0
				if avgPrice > 0:
					#print avgPrice
					count = count + 1
					totalAvg = totalAvg + avgPrice

			if count == 0 or totalAvg == 0.0:
				continue
			totalAvg = totalAvg / float(count)

			writer.writerow({'year': year, 'price': totalAvg})

			print "totalAvg is", totalAvg
			
			tcount = tcount + 1
			
		except ET.ParseError as err:
			print "Exception"

		os.remove(fileName)

if tcount == 0:
	os.remove(outputFileName)