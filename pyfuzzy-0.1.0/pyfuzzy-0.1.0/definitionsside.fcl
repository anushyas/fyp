FUNCTION_BLOCK dummy
 
    VAR_INPUT
        temp :     REAL; (* RANGE(10 .. 40) *)
        rain:    REAL; (* RANGE(300 .. 4000) *)
        ph:    REAL; (* RANGE(4.5 .. 8.5) *)
    END_VAR
 
    VAR_OUTPUT
        crop : REAL; (* RANGE(1 .. 31) *)
    END_VAR

	FUZZIFY temp
        TERM t10to25 := (10, 0) (17, 1) (19, 1) (25, 0) ;
        TERM t10to30 := (10, 0) (19, 1) (21, 1) (30, 0) ;
        TERM t10to35 := (10, 0) (20, 1) (25, 1) (35, 0) ;
        TERM t10to40 := (10, 0) (23, 1) (27, 1) (40, 0) ;
        TERM t13to21 := (13, 0) (16, 1) (18, 1) (21, 0) ;
        TERM t15to20 := (15, 0) (17, 1) (18, 1) (20, 0) ;
        TERM t16to28 := (16, 0) (21, 1) (23, 1) (28, 0) ;
        TERM t17to27 := (17, 0) (22, 1) (24, 1) (27, 0) ;
        TERM t18to20 := (18, 0) (19, 1) (20, 0) ;
        TERM t18to24 := (18, 0) (20, 1) (22, 1) (24, 0) ;
        TERM t20to24 := (20, 0) (22, 1) (24, 0) ;
        TERM t20to30 := (20, 0) (24, 1) (26, 1) (30, 0) ;
        TERM t20to33 := (20, 0) (25, 1) (28, 1) (33, 0) ;
        TERM t21to26 := (21, 0) (23, 1) (24, 1) (26, 0) ;
        TERM t22to35 := (22, 0) (27, 1) (30, 1) (35, 0) ;
        TERM t24to27 := (24, 0) (25, 1) (26, 1) (27, 0) ;
        TERM t24to35 := (24, 0) (28, 1) (31, 1) (35, 0) ;
        TERM t25to30 := (25, 0) (27.5, 1) (30, 0) ;
        TERM t25to35 := (25, 0) (29, 1) (31, 1) (35, 0) ;
        TERM t30to32 := (30, 0) (31, 1) (32, 0) ;
    END_FUZZIFY

    FUZZIFY rain
        TERM r1000 := (950, 0) (1000, 1) (1050, 0) ;
        TERM r2000 := (1950, 0) (2000, 1) (2050, 0) ;
        TERM r1000to1200 := (1000, 0) (1100, 1) (1200, 0) ;
        TERM r1000to2000 := (1000, 0) (1250, 1) (1750, 1) (2000, 0) ;
        TERM r1500to3000 := (1500, 0) (2000, 1) (2500, 1) (3000, 0) ;
        TERM r1500to4000 := (1500, 0) (2500, 1) (3000, 1) (4000, 0) ;
        TERM r300to400 := (300, 0) (350, 1) (400, 0) ;
        TERM r300to500 := (300, 0) (400, 1) (500, 0) ;
        TERM r750to1500 := (750, 0) (1000, 1) (1250, 1) (1500, 0) ;
        TERM r750to3750 := (750, 0) (1750, 1) (2750, 1) (3750, 0) ;
    END_FUZZIFY

    FUZZIFY ph
        TERM p45to55 := (4.5, 0) (4.9, 1) (5.1, 1) (5.5, 0) ;
        TERM p45to7 := (4.5, 0) (5.6, 1) (5.9, 1) (7, 0) ;
        TERM p45to8 := (4.5, 0) (6.3, 1) (6.7, 1) (8, 0) ;
        TERM p55to6 := (5.5, 0) (5.7, 1) (5.8, 1) (6, 0) ;
        TERM p55to65 := (5.5, 0) (5.9, 1) (6.1, 1) (6.5, 0) ;
        TERM p55to67 := (5.5, 0) (6, 1) (6.2, 1) (6.7, 0) ;
        TERM p55to68 := (6, 0) (5.8, 1) (6.1, 1) (6.8, 0) ;
        TERM p55to75 := (5.5, 0) (6.4, 1) (6.6, 1) (7.5, 0) ;
        TERM p57to67 := (5.7, 0) (6.1, 1) (6.3, 1) (6.7, 0) ;
        TERM p5to65 := (5, 0) (5.9, 1) (6.1, 1) (6.5, 0) ;
        TERM p62to7 := (6.2, 0) (6.6, 1) (6.7, 1) (7, 0) ;
        TERM p63to75 := (6.3, 0) (6.8, 1) (7, 1) (7.5, 0) ;
        TERM p65to75 := (6.5, 0) (6.9, 1) (7.1, 1) (7.5, 0) ;
        TERM p6to68 := (6, 0) (6.4, 1) (6.5, 1) (6.8, 0) ;
        TERM p6to7 := (6, 0) (6.4, 1) (6.6, 1) (7, 0) ;
        TERM p6to75 := (6, 0) (6.9, 1) (7.1, 1) (7.5, 0) ;
        TERM p6to8 := (6, 0) (6.9, 1) (7.1, 1) (8, 0) ;
    END_FUZZIFY
    
    DEFUZZIFY crop
        TERM beans := 1 ;
        TERM beetroot := 2 ;
        TERM bhindi := 3 ;
        TERM bittergourd := 4 ;
        TERM blackpepper := 5 ;
        TERM bottlegourd := 6 ;
        TERM brinjal := 7 ;
        TERM cabbage := 8 ;
        TERM cardamom := 9 ;
        TERM carrot := 10 ;
        TERM cashewnut := 11 ;
        TERM cauliflower := 12 ;
        TERM coriander := 13 ;
        TERM cucumber := 14 ;
        TERM drumstick := 15 ;
        TERM jackfruit := 16 ;
        TERM mango := 17 ;
        TERM orange := 18 ;
        TERM potato := 19 ;
        TERM sweetpotato := 20 ;
        TERM tapioca := 21 ;
        TERM tea := 22 ;
        TERM tomato := 23 ;
        ACCU:MAX;
        METHOD: COGS;(*MoM;*)
        DEFAULT := 0; 
    END_DEFUZZIFY

    RULEBLOCK first
        AND:MIN;
        (*ACCU:MAX;*)
        RULE 0: IF (temp IS t17to27) AND (rain IS r300to400) AND (ph IS p55to6) THEN (crop IS beans);
        RULE 1: IF (temp IS t18to24) AND (ph IS p63to75) THEN (crop IS beetroot);
        RULE 2: IF (temp IS t22to35) AND (ph IS p6to68) THEN (crop IS bhindi);
        RULE 3: IF (temp IS t24to35) AND (ph IS p55to67) THEN (crop IS bittergourd);
        RULE 4: IF (temp IS t10to40) AND (rain IS r2000) AND (ph IS p5to65) THEN (crop IS blackpepper);
        RULE 5: IF (temp IS t25to35) AND (ph IS p65to75) THEN (crop IS bottlegourd);
        RULE 6: IF (temp IS t13to21) AND (ph IS p65to75) THEN (crop IS brinjal);
        RULE 7: IF (temp IS t15to20) AND (ph IS p55to65) THEN (crop IS cabbage);
        RULE 8: IF (temp IS t10to35) AND (rain IS r1500to4000) AND (ph IS p45to7) THEN (crop IS cardamom);
        RULE 9: IF (temp IS t15to20) AND (ph IS p6to7) THEN (crop IS carrot);
        RULE 10: IF (temp IS t20to30) AND (rain IS r1000to2000) THEN (crop IS cashewnut);
        RULE 11: IF (temp IS t10to25) AND (ph IS p6to7) THEN (crop IS cauliflower);
        RULE 12: IF (temp IS t20to30) AND (ph IS p6to8) THEN (crop IS coriander);
        RULE 13: IF (temp IS t20to24) AND (ph IS p55to67) THEN (crop IS cucumber);
        RULE 14: IF (temp IS t25to30) AND (ph IS p62to7) THEN (crop IS drumstick);
        RULE 15: IF (temp IS t16to28) AND (rain IS r1500to3000) AND (ph IS p6to75) THEN (crop IS jackfruit);
        RULE 16: IF (temp IS t24to27) AND (rain IS r750to3750) AND (ph IS p55to75) THEN (crop IS mango);
        RULE 17: IF (temp IS t10to35) AND (rain IS r1000to1200) AND (ph IS p55to75) THEN (crop IS orange);
        RULE 18: IF (temp IS t18to20) AND (rain IS r300to500) AND (ph IS p5to65) THEN (crop IS potato);
        RULE 19: IF (temp IS t30to32) AND (rain IS r300to500) AND (ph IS p5to65) THEN (crop IS potato);
        RULE 20: IF (temp IS t21to26) AND (rain IS r750to1500) AND (ph IS p57to67) THEN (crop IS sweetpotato);
        RULE 21: IF (rain IS r1000) AND (ph IS p45to8) THEN (crop IS tapioca);
        RULE 22: IF (temp IS t20to33) AND (rain IS r1500to3000) AND (ph IS p45to55) THEN (crop IS tea);
        RULE 23: IF (temp IS t10to30) AND (ph IS p55to68) THEN (crop IS tomato);

    END_RULEBLOCK
 
END_FUNCTION_BLOCK