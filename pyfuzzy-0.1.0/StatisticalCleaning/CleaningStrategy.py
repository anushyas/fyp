import collections,pickle
import operator
from getTitle import getTitle
from  getStrategy import getMixedCropStrategy, dd

crop_strategy = collections.defaultdict(list)
cstrategy = collections.defaultdict(list)
cstrategylength = dict()

key_terms = [
	"sow",
	"distance",
	"harvest",
	"days",
	"economically",
	"together",
	"rainfall",
	"success",
	"temperature",
	"month",
	"production",
	"gap",
	"space",
	"length",
	"plough",
	"soil",
	"inch",
	"disease",
	"time",
	"kg/ha",
	"land",
	"yield",
	"preparation",
	"prepare",
	"litre",
	"seed",
	"spray",
	"year",
	"week",
	"fertilizer",
	"herbicide",
	"fungicide"
	"pesticide"
	"weeding",
	"season",
	"sunny",
	"rainy",
	"cultivat",
	"january",
	"february",
	"march",
	"april",
	"may",
	"june",
	"july",
	"august",
	"september",
	"october",
	"november",
	"december",
	"growth",
	"period",
	"grow",
	"crop",
	"texture",
	"kg",
	"cm",
	"humid",
	"moist",
	"condition",
	"irrigation",
	"percentage",
	"acre",
	"worm",
	"water"
]

maxleng = 0.0

with open('crop_strategy.pickle', 'rb') as handle:
	crop_strategy = pickle.load(handle)
	for key, value in crop_strategy.items():
		if getTitle(key) not in cstrategy:
			cstrategy[getTitle(key)] = list()
		for ov in value:
			v = ' '.join(ov)
			v = v.lower()
			v = v.replace(" ","")
			addv = False
			for k in key_terms:
				if k in v:
					addv = True
			if addv and ov not in cstrategy[getTitle(key)]:

				# Checking if the strategy already exists in companion.. If so, not adding it
				addm = True
				mixedStrategy = collections.defaultdict(list)
				mixedStrategy = getMixedCropStrategy(getTitle(key))
				for mixedKey, mixedValues in mixedStrategy.items():
					for mixedValue in mixedValues:
						if mixedValue == ov:
							addm = False
				if(addm):
					cstrategy[getTitle(key)].append(ov)

		cstrategylength[getTitle(key)] = len(cstrategy[getTitle(key)])
		if cstrategylength[getTitle(key)] > maxleng:
				maxleng = cstrategylength[getTitle(key)]


	for key, value in cstrategylength.items():
		if maxleng != 0:
			cstrategylength[key] = float(cstrategylength[key]) / float(maxleng)

rank = dict()

for k, v in cstrategylength.items():
	rank[k] = v

with open('synonym_crops_strategy.pickle', 'wb') as handle:
	pickle.dump(cstrategy, handle)

with open('synonym_crops_rank.pickle', 'wb') as handle:
	pickle.dump(cstrategylength, handle)
