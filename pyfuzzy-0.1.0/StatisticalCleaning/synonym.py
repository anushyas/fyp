import collections,pickle
import sqlite3
import re , collections , math , string , operator


conn = sqlite3.connect('strategy.sqlite')

def dd():
	return collections.defaultdict(list)

crop_companion = collections.defaultdict(dd)
crop_strategy = collections.defaultdict(list)

mc = conn.cursor()
mc.execute("SELECT label,evidence_candidate_id,relation_id  FROM corpus_evidencelabel")
while True:
	mrow = mc.fetchone()
	if mrow == None:
		break
	if mrow[0] != 'YE':
		continue
	cid = mrow[1]
	c = conn.cursor()
	c.execute("SELECT left_entity_occurrence_id,right_entity_occurrence_id,segment_id FROM corpus_evidencecandidate WHERE id=:Id",{"Id": cid})
	
	while True:
		row = c.fetchone()
		if row == None:
			break
		
		c2 = conn.cursor()
		uId = row[0]
		c2.execute("SELECT alias FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
		leftrow = c2.fetchone()
		crop1 = leftrow[0]#.encode("utf-8");
		
		uId = row[1]
		c2.execute("SELECT alias FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
		rightrow = c2.fetchone()
		crop2 = rightrow[0]#.encode("utf-8");
		
		c3 = conn.cursor()
		uId = row[2]
		c3.execute("SELECT document_id,offset,offset_end FROM corpus_textsegment WHERE id=:Id",{"Id": uId})
		textrow = c3.fetchone()

		c4 = conn.cursor()
		uId = textrow[0]
		c4.execute("SELECT tokens FROM corpus_iedocument WHERE id=:Id",{"Id": uId})
		text = c4.fetchone()
		text = text[0].split(',')
		strategy =  text[textrow[1]:(textrow[2]+1)]
		#print(crop1,crop2,strategy)
		if mrow[2] == 1 or mrow[2] == 2 or mrow[2] == 3:
			crop_companion[crop1][crop2].append(strategy)
			crop_companion[crop2][crop1].append(strategy)
		else:
			crop_strategy[crop1].append(strategy)
c = conn.cursor()
c.execute("SELECT entityoccurrence_id,textsegment_id FROM corpus_entityoccurrence_segments")
while True:
	row = c.fetchone()
	if row == None:
		break

	c2 = conn.cursor()
	uId = row[0]
	c2.execute("SELECT alias,entity_id FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
	rightrow = c2.fetchone()
	crop = rightrow[0]#.encode("utf-8");
	entityid = rightrow[1]

	c5 = conn.cursor()
	c5.execute("SELECT kind_id FROM corpus_entity WHERE id=:Id",{"Id": entityid})
	entitykindrow = c5.fetchone()
	entitykind = entitykindrow[0]

	if entitykind == '1':
		c3 = conn.cursor()
		uId = row[1]
		c3.execute("SELECT document_id,offset,offset_end FROM corpus_textsegment WHERE id=:Id",{"Id": uId})
		textrow = c3.fetchone()

		c4 = conn.cursor()
		uId = textrow[0]
		c4.execute("SELECT tokens FROM corpus_iedocument WHERE id=:Id",{"Id": uId})
		text = c4.fetchone()
		text = text[0].split(',')
		strategy = text[textrow[1]:(textrow[2]+1)]

		crop_strategy[crop].append(strategy)

conn.close()

def word2ngrams(text, n=3, exact=True):
	return ["".join(j) for j in zip(*[text[i:] for i in range(n)])]

def twongram_match(key, crop):
	word12 = word2ngrams(key,2)
	word22 = word2ngrams(crop,2)
	result = len(set(word12).intersection(word22))
	length = min(len(key), len(crop))
	#print "2", length, result
	if length <= 4:
		if result < length - 1:
			return False
		else:
			return True
	elif result < length - 2:
		return False
	else:
		return True

def ngram_match(key, crop):
	key = key.lower()
	crop = crop.lower()
	key = key.replace(" ", "")
	crop = crop.replace(" ", "")
	return (twongram_match(key, crop) or twongram_match(key, crop + "nut"))



title = True;
key = "";
similarCrops = dict()
listOfKeys = list()

with open('crops') as f:
    lines = f.readlines()
    for line in lines:
	    if (line != '\n') :
	    	if title:
	    		key = (line[:len(line) - 6]).capitalize()
	    		listOfKeys.append(key)
	    	similarCrops[(line[:len(line) - 6])] = key
	    	title = False;
	    else:
	    	title = True


######### for single crop strategy


sCrops = dict()

synonym_crop_strategy = collections.defaultdict(list)

for key, value in crop_strategy.items():
	if key not in similarCrops:
		f = False;
		for k in listOfKeys:
			if(ngram_match(k, key)):
				similarCrops[key] = k;		
				f = True
				break;
		if(not f):
			similarCrops[key] = key.capitalize();
			listOfKeys.append(key.capitalize())
	sCrops[key] = similarCrops[key]			
	synonym_crop_strategy[sCrops[key]].append(value)


print " "
print " "
for key, value in synonym_crop_strategy.items():
	print key, len(value)


with open('synonym_crops_strategy.pickle', 'wb') as handle:
	pickle.dump(synonym_crop_strategy, handle)


############### for mixed


synonym_mixed_crop_strategy = collections.defaultdict(dd)

###### Outer key first

for key, value in crop_companion.items():
	if key not in similarCrops:
		f = False;
		for k in listOfKeys:
			if(ngram_match(k, key)):
				similarCrops[key] = k;		
				f = True
				break;
		if(not f):
			similarCrops[key] = key.capitalize();
			listOfKeys.append(key.capitalize())
	sCrops[key] = similarCrops[key]			
	synonym_mixed_crop_strategy[sCrops[key]] = dict(synonym_mixed_crop_strategy[sCrops[key]].items() + crop_companion[key].items())


###### Inner key second

for keys, v in synonym_mixed_crop_strategy.items():
	for key, value in v.items():
		if key not in similarCrops:
			f = False;
			for k in listOfKeys:
				if(ngram_match(k, key)):
					similarCrops[key] = k;		
					f = True
					break;
			if(not f):
				similarCrops[key] = key.capitalize();
				listOfKeys.append(key.capitalize())
		sCrops[key] = similarCrops[key]			
		if sCrops[key] not in synonym_mixed_crop_strategy[keys]:
			synonym_mixed_crop_strategy[keys][sCrops[key]] = list()
		synonym_mixed_crop_strategy[keys][sCrops[key]].append(value)
		if key != sCrops[key]:
			del synonym_mixed_crop_strategy[keys][key]

for keys, v in synonym_mixed_crop_strategy.items():
	for key, value in v.items():
		print keys, key, len(value)

with open('synonym_mixed_crop_strategy.pickle', 'wb') as handle:
	pickle.dump(synonym_mixed_crop_strategy, handle)

with open('crops_title.pickle', 'wb') as handle:
	pickle.dump(sCrops, handle)


