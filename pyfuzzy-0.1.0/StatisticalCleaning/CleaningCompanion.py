import collections,pickle
import operator
from getTitle import getTitle

def dd():
	return collections.defaultdict(list)

crop_companion = collections.defaultdict(dd)
ccompanion = collections.defaultdict(dd)
ccompanionlength = dict()

ccompanioncrops = dict()

key_terms = [
	"sow",
	"distance",
	"harvest",
	"days",
	"economically",
	"together",
	"rainfall",
	"success",
	"temperature",
	"month",
	"production",
	"gap",
	"space",
	"length",
	"plough",
	"soil",
	"inch",
	"disease",
	"time",
	"kg/ha",
	"land",
	"yield",
	"preparation",
	"prepare",
	"litre",
	"seed",
	"spray",
	"year",
	"week",
	"fertilizer",
	"herbicide",
	"fungicide"
	"pesticide"
	"weeding",
	"season",
	"sunny",
	"rainy",
	"cultivat",
	"january",
	"february",
	"march",
	"april",
	"may",
	"june",
	"july",
	"august",
	"september",
	"october",
	"november",
	"december",
	"growth",
	"period",
	"grow",
	"crop",
	"texture",
	"kg",
	"cm",
	"humid",
	"moist",
	"condition",
	"irrigation",
	"percentage",
	"acre",
	"worm",
	"water"
	"intercrop",
	"mixedcrop",
	"combine",
	"rotate"
]

maxleng = 0

with open('crop_companion.pickle', 'rb') as handle:
	crop_companion = pickle.load(handle)
	for key, value in crop_companion.items():
		key = getTitle(key)
		if key not in ccompanionlength:
			ccompanionlength[key] = dict()
			ccompanioncrops[key] = list();
		for k, v in value.items():
			#print "Inner ", k
			k = getTitle(k)
			#print "Inner ", k
			if k not in ccompanion[key]:
				ccompanion[key][k] = list()
				ccompanioncrops[key].append(k);
			for v1 in v:
				#print v1
				v2 = ' '.join(v1)
				v2 = v2.lower()
				v2 = v2.replace(" ","")
				addv = False
				for key1 in key_terms:
					if key1 in v2:
						addv = True
				if addv and v1 not in ccompanion[key][k]:
					ccompanion[key][k].append(v1)

			ccompanionlength[key][k] = len(ccompanion[key][k])

			if ccompanionlength[key][k] > maxleng:
				maxleng = ccompanionlength[key][k]

	#print maxleng

	for key, value in ccompanionlength.items():
		for k,v in value.items():
			if maxleng != 0:
				ccompanionlength[key][k] = float(ccompanionlength[key][k]) / float(maxleng)
				#print key, k, ccompanionlength[key][k]

for key, val in ccompanioncrops.items():
	print key, val

with open('synonym_mixed_crops_strategy.pickle', 'wb') as handle:
	pickle.dump(ccompanioncrops, handle)

with open('synonym_mixed_crops_rank.pickle', 'wb') as handle:
	pickle.dump(ccompanionlength, handle)
