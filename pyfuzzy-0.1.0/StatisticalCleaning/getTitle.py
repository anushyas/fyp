import pickle


def word2ngrams(text, n=3, exact=True):
	return ["".join(j) for j in zip(*[text[i:] for i in range(n)])]

def twongram_match(key, crop):
	word12 = word2ngrams(key,2)
	word22 = word2ngrams(crop,2)
	result = len(set(word12).intersection(word22))
	length = min(len(key), len(crop))
	#print "2", length, result
	if length <= 4:
		if result < length - 1:
			return False
		else:
			return True
	elif result < length - 2:
		return False
	else:
		return True

def ngram_match(key, crop):
	key = key.lower()
	crop = crop.lower()
	key = key.replace(" ", "")
	crop = crop.replace(" ", "")
	return (twongram_match(key, crop) or twongram_match(key, crop + "nut"))

def getTitle(crop):
	with open('StatisticalCleaning/crops_title.pickle', 'rb') as handle:
		crop_title = pickle.load(handle)

		if crop not in crop_title:
			f = False;
			for k in crop_title.values():
				if(ngram_match(k, crop)):
					crop_title[crop] = k;		
					f = True
					break;
			if(not f):
				crop_title[crop] = crop.capitalize();

		return str(crop_title[crop])

	with open('StatisticalCleaning/crops_title.pickle', 'wb') as handle:
		pickle.dump(crop_title, handle)
