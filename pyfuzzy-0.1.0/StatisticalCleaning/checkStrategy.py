import pickle, collections, sys
from getTitle import getTitle

def dd():
	return collections.defaultdict(list)

def getCropStrategy(crop):
	strategy = collections.defaultdict(list)
	with open('crop_strategy.pickle', 'rb') as handle:
		strategy = pickle.load(handle)
		if crop in strategy:
			return strategy[crop]
		else:
			None

def getMixedCropStrategy(crop):
	strategy = collections.defaultdict(dd)
	with open('crop_companion.pickle', 'rb') as handle:
		strategy = pickle.load(handle)
		if crop in strategy:
			return strategy[crop]
		else:
			None

crop = sys.argv[1]
print getCropStrategy(crop)
#print getMixedCropStrategy(crop)