import sys,os,pickle,operator,codecs
import antlr3,fuzzy
import fuzzy.storage.fcl.Reader
from predictWeather import predict_weather
from costLinearRegression import getCost
from StatisticalCleaning.getStrategy import *
from StatisticalCleaning.getTitle import *
sys.path.insert(0,'fertpest/')
from fertilizer_recommender import *
from pesticide import *
def crop_recommendation(region):
	region_dict = dict()
	with open('region_dict.pickle', 'rb') as handle:
		region_dict = pickle.load(handle)
	synonyms = { 'arecanut' : 'Arecanut' , 'arhar' : 'Arhar - Pigeon Pea - Toor Dal - Red Gram' , 'bajra' : 'Bajra - Pearl Millet' , 
	'banana' : 'Banana' , 'barley' : 'Barley' , 'castor seeds' : 'Castor seeds' , 'chilli' : 'Chilli' ,'coconut' : 'Coconut','cotton' : 'Cotton',
	'green gram' : 'Green Gram - Moong', 'groundnut' : 'Groundnut','horse gram' : 'Horse Gram','jowar' : 'Jowar - Sorghum','jute' : 'Jute',
	'maize' : 'Maize - Corn','mustard' : 'Mustard','onion' : 'Onion','green peas' : 'Green Peas - Pea','ragi' : 'Ragi - Finger Millet',
	'rice' : 'Rice','sesame' : 'Sesame','soyabean' : 'Soyabean - Soybean','sugarcane' : 'Sugarcane','sunflower' : 'Sunflower - Oilseed',
	'tobacco' : 'Tobacco','turmeric' : 'Turmeric','urad' : 'Urad - Black Gram','wheat' : 'Wheat',
	'beans' : 'Beans - French Beans','beetroot' : 'Beetroot - Beet','bhindi' : 'Bhindi - Ladies Finger - Okra','bitter gourd' : 'Bitter Gourd',
	'black pepper' : 'Black Pepper','bottle gourd' : 'Bottle Gourd','brinjal' : 'eggplant - Brinjal','cabbage' : 'Cabbage','cardamom' : 'Cardamom',
	'carrot' : 'Carrot','cashewnut' : 'Cashewnut - Cashew','cauliflower' : 'Cauliflower','coffee' :'Coffee','coriander' : 'Coriander',
	'cucumber' : 'Cucumber','drumstick' : 'Drumstick','jackfruit' : 'Jackfruit','mango' : 'Mango','orange' : 'Orange - Citrus',
	'potato' : 'Potato','sweet potato' : 'Sweet Potato','tapioca' : 'Cassava - Tapioca','tea' : 'Tea','tomato' : 'Tomato' }
	maincrops = ['','arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','grean peas','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat']
	sidecrops = ['','beans','beetroot','bhindi','bitter gourd','black pepper','bottle gourd','brinjal','cabbage','cardamom','carrot','cashewnut','cauliflower','coriander','cucumber','drumstick','jackfruit','mango','orange','potato','sweet potato','tapioca','tea','tomato']
	# preallocate input and output values
	my_input = {
	        "temp" : 0.0,
	        "rain" : 0.0,
	        "ph" : 0.0
	        }
	my_output = {
	        "crop" : 0.0
	        }
	crops = dict()

	for month in range(0,6):
		for days in range(365,366):
			# set input values
			weatherDict = predict_weather(region)
			startmonth = 1 + month
			endmonth = 1 + month + days/30
			totalprecipitation = 0
			totalavgTemp = 0
			for i in range(startmonth,endmonth+1):
				totalprecipitation = totalprecipitation + weatherDict["precipitation"][i%12]
				totalavgTemp = totalavgTemp + weatherDict["avgTemp"][i%12]
			avgTemp = float(totalavgTemp)/float((days/30) + 1)
			my_input["temp"] = avgTemp
			my_input["rain"] = totalprecipitation

			my_input["ph"] = region_dict[region][0]

			# calculate
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsmain.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = maincrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
			
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsside.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = sidecrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
			
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsmain2.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = maincrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
			
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsside2.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = sidecrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
	#print(crops)
	ranks = dict()
	for (crop1,month) in crops.items():
		rank = (6-month)/6 * 0.3 + getCropRank(crop1) * 0.3 + getCost(crop1, 2016) * 0.4
		ranks[crop1] = rank
	result_crops = []  
	i = 0
	for key, value in sorted(ranks.items(), key=operator.itemgetter(1), reverse = True):  
		result_crops.append(key)
		i = i + 1
		if (i == 5):
			break
	#add farming strategies, mixed crops, fertilizer, pesticide for the list of crops and print
	result_strategy = dict()
	result_mixed = dict()
	result_fert = dict()
	result_pest = dict()
	for crop in result_crops:
		result_strategy[crop] = list()
		result_strategy[crop] = getCropStrategy(crop)
		result_mixed[crop] = getMixedCropStrategy(crop)
		for key,value in result_mixed[crop].items():
			ls = value
			result_mixed[crop][key] = list()
			for v in ls:
				newv = " ".join(v)
				result_mixed[crop][key].append(newv + '\n')
		newv = ""
		for v in result_strategy[crop]:
			curv = " ".join(v)
			curv = ''.join([i if ord(i) < 128 else ' ' for i in curv])
			newv = (newv + curv + '\n')
			#print newv
		newv = newv.encode('ascii')
		newv = newv.decode('ascii','ignore')
		result_strategy[crop] = newv
		result_fert[crop] = recommend_fertilizer(crop.lower(),region)
		result_pest[crop] = pest_recommend(crop.lower())
	final_result = list()
	i = 1
	for crop in result_crops:
		final_result.append( "recommendation " + str(i) + ": " + '\n' + '\n')
		final_result.append("crop recommended: " + synonyms[crop] + '\n' + '\n')
		final_result.append("fertilizer recommended: " + result_fert[crop] + '\n' + '\n')
		final_result.append("pesiticide recommended: " + str(result_pest[crop]) + '\n' + '\n')
		final_result.append("mixed cropping: " + str(result_mixed[crop]) + '\n' + '\n')
		final_result.append("farming strategy: " + str((result_strategy[crop]).decode('ascii')) + '\n' + '\n')
		i = i+1
	return final_result
'''StatisticalCleaning demo
strategy =  getCropStrategy("Maize")
mixed = getMixedCropStrategy("Maize")
mixed2 = dict()
for key,value in mixed.items():
	ls = value
	mixed2[key] = list()
	for v in ls:
		newv = " ".join(v)
		mixed2[key].append(newv + '\n')
newv = ""
for v in strategy:
	curv = " ".join(v)
	curv = ''.join([i if ord(i) < 128 else ' ' for i in curv])
	newv = (newv + curv + '\n')
print "rank: ",getCropRank("Maize")
print " "
print " "
print "Mixed cropping: "
print mixed2
print " "
print " "
print "Farming strategies: "
print newv'''

res  = crop_recommendation(sys.argv[1])
for x in res:
	print x,'\n'