import sys,os,pickle
from flask import Flask, render_template, request, redirect, send_from_directory
from fuzzyrecommendation import crop_recommendation
result = dict()
url = "index"
app = Flask(__name__)
@app.route('/suggest', methods = ['GET','POST'])
def suggest():
    region = request.form['region']
    n = -1
    p = -1
    k = -1
    year = 2016
    if(request.form['year'] != ""):
        year = int(request.form['year'])
    if(request.form['n'] != ""):
        n = float(request.form['n'])
    if(request.form['p'] != ""):
        p = float(request.form['p'])
    if(request.form['k'] != ""):
        k = float(request.form['k'])
    print n, p ,k
    url = "suggest"
    print("The region is '" + region + "'")
    result,weather = crop_recommendation(str(region),year,n,p,k)
    print result,weather
    #result = '<br/>'.join(result.split('\n'))
    return render_template('index.html', result=result, weather = weather ,url = url)
@app.route("/", methods = ['GET','POST'])
@app.route("/index",methods=['GET','POST'])
def main():
    return render_template('index.html')
if __name__ == "__main__":
	app.debug = True
	app.run(processes=1)
