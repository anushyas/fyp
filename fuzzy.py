import sys,os,pickle
import antlr3,fuzzy
import fuzzy.storage.fcl.Reader
system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitions.fcl")
 
# preallocate input and output values
my_input = {
        "temp" : 0.0,
        "rain" : 0.0,
        "soil" : 0.0,
        "ph" : 0.0,
        "depth" : 0.0
        }
my_output = {
        "crop" : 0.0
        }

# set input values
my_input["temp"] = 31
my_input["rain"] = 1500
my_input["soil"] = 1
my_input["ph"] = 6
my_input["depth"] = 80

# calculate
system.calculate(my_input, my_output)

# now use outputs
print my_output["crop"]


with open('crop_companion.pickle', 'rb') as handle:
  crop_companion = pickle.load(handle)

with open('crop_strategy.pickle', 'rb') as handle:
  crop_strategy = pickle.load(handle)

for crop2 in crop_companion[]